#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Utilisation : $0 <dossier sortie>"
    exit 1
fi

find . -type f \! -name '*.sh' | while read file; do
    file="${file:2}"
    width=$(identify -format "%w" "$file")
    height=$(identify -format "%h" "$file")
    
    test=$(echo "($width/$height)>(16/9)" | bc -l)

    if [[ "$test" -gt 0 ]]; then
        height=$(echo "($width * 9) / 16" | bc)
    else
        width=$(echo "($height * 16) / 9" | bc)
    fi

    dir=$(dirname "$file")
    bname=$(basename "$file")
    mkdir -p "$1/$dir"
    
    echo "$file -> $1/$dir/$bname"

    convert "$file" -background black -gravity center -extent "$width"x"$height" "$1/$dir/$bname"
done

